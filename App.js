import React, { Component } from 'react';
import { Button, Image } from 'react-native';
import { NavigationContainer, NavigationActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './Views/Home';
import Inscription from './Views/Inscription';
import VoirRdv from './Views/VoirRdv';
import Connexion from './Views/Connexion';
import PrendreRdv from './Views/PrendreRdv';
import User from './Views/User';
import ShowMedecin from './Views/ShowMedecin';

const Stack = createStackNavigator();

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={Home} 
            options={({ navigation }) => ({
              headerLeft: () => (
                <Image
                  style={styles.iconLeft}
                  source={{
                    uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                  }}
                />
              ),title: "",
            })}
          />
          <Stack.Screen name="Inscription" component={Inscription} 
          options={({ navigation }) => ({
            headerRight: () => (
              <Image
                style={styles.iconRight}
                source={{
                  uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                }}
              />
            ),title: "",
          })}
          />
          <Stack.Screen name="Connexion" component={Connexion} 
          options={({ navigation }) => ({
            headerRight: () => (
              <Image
                style={styles.iconRight}
                source={{
                  uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                }}
              />
            ),title: "",
          })}
          />
          <Stack.Screen name="User" component={User} options={({ navigation }) => ({
            headerRight: () => (
              <Button
                onPress={() => navigation.navigate("Home")}
                title="Deconnexion"
              />
            ),
            headerLeft: () => (
              <Image
                style={styles.iconLeft}
                source={{
                  uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                }}
              />
            ),title: ""
          })}
          />
          <Stack.Screen name="VoirRdv" component={VoirRdv} options={({ navigation }) => ({
            headerTitle: () => (
              <Image
                style={styles.iconLeft}
                source={{
                  uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                }}
              />
            ),
            headerRight: () => (
              <Button
                onPress={() => navigation.navigate("Home")}
                title="Deconnexion"
                style={styles.button}
              />
            ), title: "Liste de mes rendez-vous"
          })}
          />
          <Stack.Screen name="PrendreRdv" component={PrendreRdv} options={({ navigation }) => ({
            headerTitle: () => (
              <Image
                style={styles.iconLeft}
                source={{
                  uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                }}
              />
            ),
            headerRight: () => (
              <Button
                onPress={() => navigation.navigate("Home")}
                title="Deconnexion"
              />
            ), title: ""
          })}
          />
          <Stack.Screen name="ShowMedecin" component={ShowMedecin} options={({ navigation }) => ({
            headerTitle: () => (
              <Image
                style={styles.iconLeft}
                source={{
                  uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                }}
              />
            ),
            headerRight: () => (
              <Button
                onPress={() => navigation.navigate("Home")}
                title="Deconnexion"
              />
            ), title: ""
          })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = {
  iconTitle: {
    width: 60,
    height: 37,
  },
  iconLeft: {
    width: 60,
    height: 37,
    marginLeft: 10,
  },
  iconRight: {
    width: 60,
    height: 37,
    marginRight: 10,
  },
  button: {
    marginRight: 10,
  }
};