ETAPES A EFFECTUER AFIN DE POUVOIR DEPLOYER CE PROJET SUR VOTRE SMARTPHONE :

1.Cloner le projet sur votre IDE

2.Suivre ce tutoriel afin d'installer les logiciels nécéssaires au bon fonctionnement du projet
https://www.developers-zone.com/start-react-native-without-expo/

3.Brancher votre smartphone a votre ordinateur

4.Mettre votre smartphone en mode développement dans les paramètres

5.Ouvrir une invite de commande dans le projet et entrer les commandes suivantes :

adb devices (afin d'obtenir le code de votre smartphone)

adb -s <code de votre smartphone> reverse tcp:5037 tcp:5037

npx react-native start (afin de lancer react-native)

6.Ouvrir un second terminal dans votre projet et entrer la commande suivante :

npx react-native run-android

7.Créez un compte et commencez a prendre des rendez-vous !
