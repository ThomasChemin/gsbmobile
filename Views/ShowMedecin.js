import React, { Component } from "react";
import { SafeAreaView } from "react-native";
import { FlatList, TextInput, Alert, View, Text, Button } from 'react-native';
import { ScrollView } from "react-native-gesture-handler";
import SQLite from 'react-native-sqlite-storage';  

const db = SQLite.openDatabase({
    name: "GSBMobileBDD.db",
    location: "default",
    createFromLocation: "~GSBMobileBDD.db",
},
);

export default class ShowMedecin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errors: [],
            medecins: this.props.route.params.medecins,
            user: this.props.route.params.user,
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <SafeAreaView>
                <Text style={style.title}>Choisir le medecin du rendez-vous</Text>
                <Text style={style.error}>
                    {
                        this.state.errors.map((item, i) => (
                            <Text key={i}>{item.ID}</Text>
                        ))
                    }
                </Text>
                <ScrollView>
                    <FlatList
                        data={this.state.medecins}
                        renderItem={({ item }) => <Button
                            key={item.id}
                            title={item.firstName + " " + item.lastName}
                            onPress={() => {
                                navigate("PrendreRdv", {
                                    user: this.state.user,
                                    medecin: item,
                                });
                            }}
                        />}
                    />
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const style = {
    error: {
        color: "red",
    },
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
        marginTop: 20,
        textAlign: 'center',
    },
    textInput: {
        width: 200,
        height: 44,
        padding: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
    button: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
}