import React, { Component,useState } from "react";
import { View, Text, Button, Image } from 'react-native';

export default function Home({ navigation }) {
    return (
        <View style={style.view}>
            <Image
                style={style.image}
                source={{
                    uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                  }}
            />
            <View style={style.button}>
                <Text style={style.text}>Connectez vous</Text>
                <Button
                    title="Connexion"
                    onPress={() => navigation.navigate('Connexion')}
                />
                <Text>{'\n'}</Text>
                <Text style={style.text}>Vous n'avez pas de compte ?</Text>
                <Text style={style.text}>Inscrivez vous</Text>
                <Button
                    title="Inscription"
                    onPress={() => navigation.navigate('Inscription')}
                />
            </View>
        </View>
    );
}

const style = {
    view: {
        flex: 1,
        alignItems: "center",
        justifyContent: 'center',
    },
    button: {
        width: 250,
    },
    image: {
        width: 260,
        height: 160,
        marginBottom: 60,
    },
    text: {
       textAlign: "center",
       fontWeight: "bold",
       fontSize: 16,
       marginBottom: 8,
    },

};