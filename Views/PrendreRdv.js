import React, { Component } from "react";
import { SafeAreaView, TextInput, View, Text, Button, Alert } from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import DatePicker from 'react-native-date-picker';
import DropDownPicker from 'react-native-dropdown-picker';
import Moment from 'moment';
import { color } from "react-native-reanimated";
import Icon from 'react-native-vector-icons/Feather';
import { ScrollView } from "react-native-gesture-handler";

const db = SQLite.openDatabase({
    name: "GSBMobileBDD.db",
    location: "default",
    createFromLocation: "~GSBMobileBDD.db",
})

export default class PrendreRdv extends Component {

    constructor(props) {

        super(props);
        this.state = {
            rdv: this.props.route.params.rdv,
            errors: [],
            hours: '',
            description: '',
            user: this.props.route.params.user,
            listMedecin: this.props.route.params.listMedecin,
            medecin: [],
            date: new Date(),
            time: new Date(),
            listMedicament: this.props.route.params.listMedicament,
            medicament: "",
        };
    };

    doPrendreRdv = () => {
        var newDate = Moment(this.state.date).format('YYYY-MM-DD');
        var newTime = Moment(this.state.time).format('hh:mm');
        var description = this.state.description;
        var idMedecin = this.state.medecin;
        var idUser = this.state.user[0].id;
        var idMedicament = this.state.medicament;
        var arrayVerif = [newDate, newTime, description, idMedecin, idUser, idMedicament];
        var isOK = true;
        arrayVerif.map((data) => {
            if (data == "" || data == null) {
                isOK = false;
                this.setState({
                    errors: [{ "field": "Tous les champs doivent être remplis !" }],
                });
            }
        });
        var newRdv = [{
            "days": newDate,
            "description": description,
            "hours": newTime,
            "id": "",
            "idMedecin": idMedecin,
            "idMedicament": idMedicament,
            "idUser": idUser,
        }];
        if (isOK) {
            db.transaction(function (tx) {
                tx.executeSql(
                    'INSERT INTO Rdv (days, hours, description, idMedecin, idUser, idMedicament) VALUES (?,?,?,?,?,?)',
                    [newDate, newTime, description, idMedecin, idUser, idMedicament],
                    (tx, results) => {
                        if (results.rowsAffected > 0) {
                            newRdv.id = results.insertId;
                            console.log(newRdv.id);
                            Alert.alert(
                                'Succès',
                                'Le rendez-vous a bien été pris',
                                [
                                    {
                                        text: "OK", onPress: () => {
                                            console.log(newRdv);
                                        }
                                    }
                                ]
                            );
                        }
                    }
                );
            });
        }
    };

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={style.view}>
                <ScrollView>
                    <Text style={style.title}>
                        PRENDRE UN RENDEZ-VOUS
                    </Text>
                    <DropDownPicker
                        items={this.state.listMedecin}
                        defaultNull
                        placeholder="CHOISIR UN MEDECIN"
                        placeholderStyle={{ color: "#ffffff" }}
                        style={{ backgroundColor: '#50a9f6' }}
                        itemStyle={{
                            justifyContent: 'flex-start',
                            color: "#ffffff",
                        }}
                        labelStyle={{ color: "#ffffff", paddingLeft: 50 }}
                        dropDownStyle={{ backgroundColor: '#60a9f6' }}
                        onChangeItem={item => this.setState({
                            medecin: item.id
                        })}
                    />
                    <DropDownPicker
                        items={this.state.listMedicament}
                        defaultNull
                        placeholder="CHOISIR UN MEDICAMENT"
                        placeholderStyle={{ color: "#ffffff" }}
                        style={{ backgroundColor: '#50a9f6' }}
                        itemStyle={{
                            justifyContent: 'flex-start',
                            color: "#ffffff",
                        }}
                        labelStyle={{ color: "#ffffff", paddingLeft: 50 }}
                        dropDownStyle={{ backgroundColor: '#60a9f6' }}
                        onChangeItem={item => this.setState({
                            medicament: item.id
                        })}
                    />
                    <Text style={style.text}>DATE & HORAIRE</Text>
                    <DatePicker
                        date={this.state.date}
                        onDateChange={(date) => this.setState({ date })}
                        mode="date"
                        style={style.date}
                    />
                    <DatePicker
                        date={this.state.time}
                        onDateChange={(time) => this.setState({ time })}
                        mode="time"
                        style={style.time}
                    />
                    <TextInput
                        placeholder="Description"
                        onChangeText={(description) => this.setState({ description })}
                        label="description"
                        value={this.state.description}
                        style={style.textInput} />
                    <View style={style.button}>
                        <Text style={style.error} >
                            {
                                this.state.errors.map((item, i) => (
                                    <Text key={i}>{item.field}</Text>
                                ))
                            }
                        </Text>
                        <Button
                            title='Valider'
                            onPress={this.doPrendreRdv.bind(this)} />
                        <Text></Text>
                        <Button
                            title="Retour"
                            onPress={() => navigate('User')} />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const style = {
    error: {
        color: "red",
    },
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center',
    },
    text: {
        fontSize: 18,
        fontWeight: "bold",
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center',
    },
    textInput: {
        padding: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 5,
        height: 45,
    },
    button: {
        padding: 10,
        borderColor: 'black',
        marginBottom: 10,
    },
    date: {
        width: 360,
    },
    time: {
        width: 360,
        marginBottom: 10
    }
}