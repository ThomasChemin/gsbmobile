import React, { Component, useState } from "react";
import { TextInput, Alert, TouchableOpacity, View, Text, Button } from 'react-native';
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({
    name: "GSBMobileBDD.db",
    location: "default",
    createFromLocation: "~GSBMobileBDD.db",
},
);

export default class Connexion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            errors: [],
            email: '',
            password: '',
        };
        
    }

    doConnection() {
        const { email, password } = this.state;
        const { navigate } = this.props.navigation;
        const query = `SELECT * FROM Users WHERE email=? AND password=?`;
        const bind = [email, password]
        db.transaction(tx => {
            tx.executeSql(query, bind, (tx, results) => {
                let length = results.rows.length;
                if (length > 0) {
                    let arrayHelper = [];
                    for (let i = 0; i < results.rows.length; i++) {
                        arrayHelper.push(results.rows.item(i));
                    }
                    navigate('User', {
                        user: arrayHelper,
                    })
                }
                else {
                    this.setState({
                        errors: [{ "ID": "Les identifiants saisis ne sont pas bons." }]
                    });
                }
            })
        })
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={style.view}>
                <Text style={style.title}>
                    SE CONNECTER{"\n"}
                </Text>
                <View>
                    <Text style={style.error}>
                        {
                            this.state.errors.map((item, i) => (
                                <Text key={i}>{item.ID}</Text>
                            ))
                        }
                    </Text>
                </View>
                <TextInput
                    placeholder="Email"
                    onChangeText={(email) => this.setState({ email })}
                    label="Email"
                    value={this.state.email}
                    style={style.textInput} />
                <Text></Text>
                <TextInput
                    placeholder="Mot de passe"
                    secureTextEntry={true}
                    onChangeText={(password) => this.setState({ password })}
                    label="Mot de passe"
                    value={this.state.password}
                    style={style.textInput} />
                <Text>{'\n'}</Text>
                <View>
                    <Button
                        title='Connexion'
                        style={style.button}
                        onPress={this.doConnection.bind(this)} />
                    <Text></Text>
                    <Button
                        title="Je n'ai pas de compte"
                        style={style.button}
                        onPress={() => navigate('Inscription')} />
                </View>
            </View>
        );
    }
}

const style = {
    error: {
        color: "red",
    },
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    title: {
        fontFamily: 'Arial',
        fontSize: 30,
        fontWeight: "bold",
        marginBottom: 20,
    },
    textInput: {
        width: 240,
        height: 44,
        padding: 10,
        textAlign: 'center',
        fontWeight: '200',
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 5,
    },
    button: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
}