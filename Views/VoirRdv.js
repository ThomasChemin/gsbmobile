import React, { Component } from "react";
import { View, Text, Button, SafeAreaView, ScrollView, Alert } from 'react-native';
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({
    name: "GSBMobileBDD.db",
    location: "default",
    createFromLocation: "~GSBMobileBDD.db",
},
);

export default class VoirRdv extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: this.props.route.params.user,
            rdv: this.props.route.params.rdv,
            errors: [],
        };

    }

    doDeleteRdv(id) {
        const { push } = this.props.navigation;
        const idSearch = id;
        var indexOfRdv;
        this.state.rdv.map((r, i) => {
            if (r.id == idSearch) {
                indexOfRdv = this.state.rdv.indexOf(r);
                this.state.rdv.splice(indexOfRdv, 1);
            }
        });
        db.transaction(tx => {
            tx.executeSql("DELETE FROM Rdv WHERE id=?", [id], (tx, results) => {
                if (results.rowsAffected > 0) {
                    Alert.alert(
                        'Succès',
                        'Vous avez bien supprimé le rendez-vous !',
                        [
                            {
                                text: "OK", onPress: () => {
                                    if (this.state.rdv.length == 0) {
                                        push("User", {
                                            user: this.state.user
                                        });
                                    }
                                    else {
                                        this.forceUpdate();
                                    }
                                }
                            }
                        ]
                    );
                }
            }, (tx, error) => {

            })
        });
    };

    componentDidMount() {
        this.setState({
            rdv: this.props.route.params.rdv,
        });
    }

    render() {
        return (
            <SafeAreaView>
                <View>
                    {
                        this.props.route.params.user.map((item, i) => (
                            <Text style={style.title} key={i}>Liste des rendez-vous de {item.firstName + " " + item.lastName}</Text>
                        ))
                    }
                    <View style={style.error}>
                        {
                            this.state.errors.map((item, i) => (
                                <Text key={i}>{item.NoRdv}</Text>
                            ))
                        }
                    </View>
                </View>
                <ScrollView style={style.scrollView}>
                    {
                        this.state.rdv.map((item, i) => (
                            <View key={i} style={style.box}>
                                <Text style={style.textLeft}>Date</Text>
                                <Text style={style.textCenter}>{item.days}</Text>
                                <Text style={style.textLeft}>Horaire</Text>
                                <Text style={style.textCenter}>{item.hours}</Text>
                                <Text style={style.textLeft}>Medecin</Text>
                                <Text style={style.textCenter}>{item.idMedecin.lastName + " " + item.idMedecin.firstName}</Text>
                                <Text style={style.textLeft}>Motif</Text>
                                <Text style={style.textCenter}>{item.description}</Text>
                                <Text style={style.textLeft}>Medicament</Text>
                                <Text style={style.textCenter}>{item.idMedicament.name}</Text>
                                <View style={style.buttonLeft}>
                                    <Button
                                        title='Supprimer'
                                        onPress={
                                            () => {
                                                this.doDeleteRdv(item.id);
                                            }
                                        }
                                    />
                                </View>
                                <View style={style.buttonRight}>
                                    <Button
                                        title='Modifier'
                                    />
                                </View>
                            </View>
                        ))

                    }
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const style = {
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    title: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: "bold",
    },
    textInput: {
        width: 200,
        height: 44,
        padding: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
    box: {
        marginBottom: 10,
        borderWidth: 2,
        borderColor: 'black',
    },
    textLeft: {
        backgroundColor: "#b8b8b8",
        color: "white",
        fontSize: 18,
        borderWidth: 1,
        borderColor: 'black',
        width: 200,
        height: 30,
        fontWeight: "bold",
        padding: 4,
    },
    textCenter: {
        backgroundColor: "white",
        color: "black",
        fontSize: 16,
        borderWidth: 1,
        borderColor: 'black',
        width: 256,
        height: 30,
        textAlign: 'left',
        marginLeft: 100,
        marginTop: -30,
        padding: 4,
    },
    buttonLeft: {
        width: 180,
        backgroundColor: 'black',
        borderWidth: 1,
        borderColor: 'black',
    },
    buttonRight: {
        width: 176,
        marginTop: -37,
        marginLeft: 180,
        backgroundColor: 'black',
        borderWidth: 1,
    },
    scrollView: {
        marginTop: 20,
        marginBottom: 40,
    }
}