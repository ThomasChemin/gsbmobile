import React, { Component } from "react";
import { TextInput, View, Alert, Text, Button } from 'react-native';
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({
    name: "GSBMobileBDD.db",
    location: "default",
    createFromLocation: "~GSBMobileBDD.db",
})

export default class Inscription extends Component {

    constructor(props) {

        super(props);
        this.state = {
            email: '',
            lastName: '',
            firstName: '',
            adresse: '',
            city: '',
            phone: '',
            password: '',
        };
    }

    doInscription = () => {
        var that = this;
        var email = this.state.email;
        var lastName = this.state.lastName;
        var firstName = this.state.firstName;
        var adresse = this.state.adresse;
        var city = this.state.city;
        var phone = this.state.phone;
        var password = this.state.password;
        db.transaction(function (tx) {
            tx.executeSql(
                'INSERT INTO Users (email, lastName, firstName, adresse, city, phone, password) VALUES (?,?,?,?,?,?,?)',
                [email, lastName, firstName, adresse, city, phone, password],
                (tx, results) => {
                    console.log('Results', results.rowsAffected);
                    if (results.rowsAffected > 0) {
                        Alert.alert(
                            'Success',
                            'You are Registered Successfully',
                            [
                                {
                                    text: 'Ok',
                                    onPress: () =>
                                        that.props.navigation.navigate('Home'),
                                },
                            ],
                            { cancelable: false }
                        );
                    } else {
                        alert('Registration Failed');
                    }
                }
            );
        });
    };

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={style.view}>
                <Text style={style.title}>
                    S'inscrire
                </Text>
                <TextInput
                    placeholder="Email"
                    onChangeText={(email) => this.setState({ email })}
                    label="Email"
                    value={this.state.email}
                    style={style.textInput} />
                <TextInput
                    placeholder="Nom de l'utilisateur"
                    onChangeText={(lastName) => this.setState({ lastName })}
                    label="lastName"
                    value={this.state.lastName}
                    style={style.textInput} />
                <TextInput
                    placeholder="Prenom de l'utilisateur"
                    onChangeText={(firstName) => this.setState({ firstName })}
                    label="firstName"
                    value={this.state.firstName}
                    style={style.textInput} />
                <TextInput
                    placeholder="Adresse de l'utilisateur"
                    onChangeText={(adresse) => this.setState({ adresse })}
                    label="adresse"
                    value={this.state.adresse}
                    style={style.textInput} />
                <TextInput
                    placeholder="Ville de l'utilisateur"
                    onChangeText={(city) => this.setState({ city })}
                    label="city"
                    value={this.state.city}
                    style={style.textInput} />
                <TextInput
                    placeholder="Telephone de l'utilisateur"
                    onChangeText={(phone) => this.setState({ phone })}
                    label="phone"
                    value={this.state.phone}
                    style={style.textInput} />
                <TextInput
                    placeholder="Mot de Passe"
                    secureTextEntry={true}
                    onChangeText={(password) => this.setState({ password })}
                    label="password"
                    value={this.state.password}
                    style={style.textInput} />
                <View style={style.button}>
                    <Button
                        title='Valider'
                        style={style.button}
                        onPress={this.doInscription.bind(this)} />
                    <Text></Text>
                    <Button
                        title="J'ai déjà un compte"
                        style={style.button}
                        onPress={() => navigate('Connexion')} />
                </View>
            </View>
        );
    }
}

const style = {
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 40,
    },
    textInput: {
        width: 240,
        height: 44,
        padding: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 5,
    },
    button: {
        width: 240,
        height: 44,
        padding: 10,
        borderColor: 'black',
        marginBottom: 10,
    },
}