import React, { Component } from "react";
import { View, Text, Button, Image } from 'react-native';
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({
    name: "GSBMobileBDD.db",
    location: "default",
    createFromLocation: "~GSBMobileBDD.db",
},
);

export default class User extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: this.props.route.params.user,
            rdv: [],
            errors: [],
            medecins: [],
            medicaments: [],
        };
        db.transaction(tx => {
            tx.executeSql(
                'SELECT * FROM Medecin ORDER BY lastName',
                [],
                (tx, results) => {
                    let length = results.rows.length;
                    if (length > 0) {
                        let arrayHelper = [];
                        for (let i = 0; i < length; i++) {
                            arrayHelper.push(results.rows.item(i));
                        }
                        this.setState({
                            medecins: arrayHelper,
                        });
                    };
                });
        });
        db.transaction(tx => {
            tx.executeSql(
                'SELECT * FROM Medicament ORDER BY name',
                [],
                (tx, results) => {
                    let length = results.rows.length;
                    if (length > 0) {
                        let arrayHelper = [];
                        for (let i = 0; i < length; i++) {
                            arrayHelper.push(results.rows.item(i));
                        }
                        this.setState({
                            medicaments: arrayHelper,
                        });
                    };
                });
        });
        db.transaction(tx => {
            tx.executeSql(
                'SELECT * FROM Rdv WHERE idUser = ? ORDER BY days',
                [this.state.user[0].id],
                (tx, results) => {
                    let length = results.rows.length;
                    if (length > 0) {
                        let arrayHelper = [];
                        for (let i = 0; i < length; i++) {
                            arrayHelper.push(results.rows.item(i));
                            this.setState({
                                rdv: arrayHelper,
                            });
                            db.transaction(xt => {
                                xt.executeSql(`SELECT * FROM Medecin WHERE id = ?`, [arrayHelper[i].idMedecin], (xt, resultsSecond) => {
                                    let lengthSecond = resultsSecond.rows.length;
                                    if (lengthSecond > 0) {
                                        for (let j = 0; j < lengthSecond; j++) {
                                            arrayHelper[i].idMedecin = resultsSecond.rows.item(j);
                                        }
                                        this.setState({
                                            rdv: arrayHelper,
                                        });
                                    };
                                });
                            });
                            db.transaction(xxt => {
                                xxt.executeSql(`SELECT * FROM Medicament WHERE id = ?`, [arrayHelper[i].idMedicament], (xxt, resultsThird) => {
                                    let lengthThird = resultsThird.rows.length;
                                    if (lengthThird > 0) {
                                        for (let j = 0; j < lengthThird; j++) {
                                            arrayHelper[i].idMedicament = resultsThird.rows.item(j);
                                        }
                                        this.setState({
                                            rdv: arrayHelper,
                                        });
                                    };
                                });
                            });
                        }
                    };
                });
        });
    }

    componentDidMount() {
        if(!typeof(this.props.route.params.rdv) == "undefined"){
            console.log("ok ?????");
            this.setState({
                rdv: this.props.route.params.rdv,
            });
        }
    }

    showRdvFromUser() {
        if (this.state.rdv.length != 0) {
            const { navigate } = this.props.navigation;
            navigate("VoirRdv", {
                user: this.state.user,
                rdv: this.state.rdv,
            });
        } else {
            this.setState({
                errors: [{ "NoRdv": "Vous n'avez pas de rendez-vous de prévu." }]
            });
        }

    };

    showPrendreRdv() {
        const { navigate } = this.props.navigation;
        var label = "";
        this.state.medecins.map((medecin) => {
            label = medecin.lastName + " " + medecin.firstName;
            medecin.label = label;
        });
        this.state.medicaments.map((medicament) => {
            label = medicament.name;
            medicament.label = label;
        });
        navigate("PrendreRdv", {
            user: this.state.user,
            listMedecin: this.state.medecins,
            listMedicament: this.state.medicaments,
            rdv: this.state.rdv,
        });

    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={style.view} >
                <Image
                    style={style.image}
                    source={{
                        uri: 'https://fchevalierblog.files.wordpress.com/2017/04/gsb.png?w=640',
                    }}
                />
                <Text>{"\n\n\n\n"}</Text>
                {
                    this.props.route.params.user.map((item, i) => (
                        <Text style={style.title} key={i}>Bienvenue sur GSB, {item.firstName + " " + item.lastName}</Text>
                    ))
                }
                < Text style={style.error} >
                    {
                        this.state.errors.map((item, i) => (
                            <Text key={i}>{item.NoRdv}</Text>
                        ))
                    }
                </Text>
                <View>
                    <Button
                        title="Voir mes rendez-vous"
                        onPress={this.showRdvFromUser.bind(this)}
                    />
                    <Text>{"\n"}</Text>
                    <Button
                        title="Prendre un rendez-vous"
                        onPress={this.showPrendreRdv.bind(this)}
                    />
                </View>
            </View>
        );
    }
}

const style = {
    error: {
        color: "red",
    },
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 50,
    },
    textInput: {
        width: 200,
        height: 44,
        padding: 10,
        textAlign: 'center',
        fontWeight: 'bold',
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
    button: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
    image: {
        width: 250,
        height: 155,
    },
}